# Limit Concept
* Protect the overall health of a cluster, if there's a misbehaving container then it will have to pay the price

## Memory Limits
* If the actual **MEMORY** usage of the container at run time exceeds the limit..
-> The **CONTAINER** will be killed (pod remain, but container restart)

## CPU Limits
* If the actual **CPU** usage of the container at run time exceeds the limit...
-> The **CPU** will be "clamped" - bóp lại (container still run)