# Scheduling
 
## How scheduling works
* What to schedule? scheduler goes through all the pods and look for those that do not have *nodeName* prop
    * -> candidates for scheduling
* Which node to schedule?
    * The scheduler goes through two phases to identify the best node for the pod
        * Filter nodes: filter out the nodes that do not fit the profile for this pod
        * Rank nodes: priority function to assign a score to the nodes on a scale of 0 to 10 (e.g.
        calculates the amount of resource that would be free on the nodes after placing the pod on them)
* Bind Pod to Node: setting the *nodeName* prop = name of the node by creating a binding object

# No Scheduler!
* Pods in pending state
* Manually assign using *Binding* Object (cuz k8s doesn't allow modify *nodeName* prop)

# Labels, Selectors, Annotations
* Annotations in metadata: used for some kind of integration purpose (name, build number, contact details, email,...)

# Static Pods
* If no other control plane component(etcd cluster, kube-api server,...). Kubelet can create and manage pods themselves by:
    * Put pod yaml file definition to /etc/kubernetes/manifests (pod-manifest-path) in worker nodes (kubelet install as services)
    * Kubelet works at POD level and only understand POD (so no deployment, replicasets,..)
* Use case:
* kubeadm tool set's up cluster by using static pods:
    * Install kubelet in each node
    * Create k8s control plane component (api-server, etcd, controller) as pod on kube-system ns in kubelet's node