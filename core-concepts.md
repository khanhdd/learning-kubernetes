# Cluster Architecture

**K8s's purpose**
* Host your applications in the form of containers in an automated fashion:
    * Easily deploy as many instances of your application as required 
    * Easily enable communication between different services within your application

**ETCD Server**
* ETCD is a distributed reliable key-value store that is Simple, Secure, Fast
* ETCD's role in k8s:
    * Stores info: Nodes, Pods, Configs, Secrets, Accounts, Roles, Bindings...
    * Kubectl get command from ETCD server
    * Every change make to cluster updated in the ETCD server

**Kube-API Server**
* Primary management component in k8s
* Only component that interacts directly with the etcd datastore
* When you run kubectl command, kubectl ultility is infact reaching kube-api server:
    * Kube-api first authenticates the request
    * Kube-api validates it 
    * Retrieve data
    * Update ETCD
    * Kube-scheduler
    * Kubelet

**Kube-controller Manager**
* Process that continuously monitors the state of various components within the system and works towards bringing the whole system to the desired functioning state

**Kube scheduler**
* Only responsible for deciding which pod goes on which node. It doesn't actually place the pod on the nodes (kubelet's job)
* The scheduler goes through two phases to identify the best node for the pod
    * Filter nodes: filter out the nodes that do not fit the profile for this pod
    * Rank nodes: priority function to assign a score to the nodes on a scale of 0 to 10 (e.g.
     calculates the amount of resource that would be free on the nodes after placing the pod on them)

**Kubelet** 
* In the kubernetes worker node
    * Registers the node with the kubernetes cluster
    * Interact with container runtime (docker, rkt,..) and Create pods
    * Monitor Node & PODs

**Kube-proxy**
* Process that runs on each node in the k8s cluster
* It's job is to look for new services and every time a new service is created it creates the appropriate rules (iptables,..) on each node to forward traffic to those services to the backend pods.
