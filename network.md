# Networking and Service Discovery

## Why:
E.g: App need store its data in database

## How:
* **Scenario 1 (Not Recommend)**: Multiple containter inside a pod
-> Difficult to manage a pod (e.g if pod fail -> app container fail? or database container fail?)

* **Scenario 2 (Recommend)**: Seperate pods -> Seperate services -> kube-dns help tie service together

## Extra Secret:
* How does your container know where to find kube-dns service ?
Answer: Automatic configuration k8s doing behind with all of our containers. cat /etc/resolv.conf (that files config how dns name resolution is going to work)

* Using fully qualified domain names (FQDN) in code when refer to service in k8s with structure service-name.namespace.svc.cluster.local (e.g. database.default.svc.cluster.local)