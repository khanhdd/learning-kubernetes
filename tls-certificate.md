# TLS Certificate
* used to guarantee trust between two parties during a transaction

* Symmetric Encryption
    * Same key for encrypt and decrypt

* Asymmetric Encryption
    * Private key and public key
    * Web-server scenario (openssl):
        * Key-pair on server
        * User access web-server https, public key from server send to user (also with certificate)
        * User's browser encrypt the symmetric key (a set of random number, a-z) using public key provided by web-server
        * Send it to server, server using it's private key to decrypt

* CA (Certificate Authority)
    * well known organizations that can sign and validate your certificate for you (Symantec, Digicert, Comodo, GlobalSign,...)
    * The way it works:
        * You generate a certificate signing request (CSR) using key generated earlier and the domain name of your Website
        * Sent to the CA for signing