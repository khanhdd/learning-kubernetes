# Readiness and Liveness Probes (aka HeathChecks) in Kubernetes

## Readiness Probes

* Perform heath checks when pod (container) start

## Liveness Probes

* Perform heath checks continually run for the duration of your pod's lifetime 
* If any reason it's fail -> k8s mark whole pod as failed and restart container