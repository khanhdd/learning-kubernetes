# Cluster Maintenance

## OS Upgrades
<!-- Drain node: the pods are gracefully terminated from the node that they're on and recreate on other -->
<!-- The node is also cordoned (cô lập) and marked as unschedulable  -->
kubectl drain node-1
kubectl uncordon node-1

<!-- Marked node unschedulable, do not terminated and move pods to another node -->
kubectl cordon node-1
kubectl uncordon node-1

## Cluster Update Process 
* Version upgrade for core control plane component:
    * kube-apiserver: vX (v1.10)
    * controller-manager: vX-1 (v1.9 or 1.10)
    * kube-scheduler: vX-1 (v1.9 or 1.10)
    * kubelet: vX-2 (v1.8, v1.9 or 1.10)
    * kube-proxy: vX-2 (v1.8, v1.9 or 1.10)
    * kubectl: vX, vX+1, vX-1
* Recommend: upgrade one minor version at a time (v1.11 -> v1.12 -> v1.13)
* Notice: when v1.13 released, only v1.11, v1.12, v1.13 are supported

###  Update strategy
* Upgrade master nodes
* Upgrade worker nodes
    * Strategy - 1: down all worker nodes, upgrade, go back online - not recommend
    * Strategy - 2: drain each nodes and upgrade, go back online
    * Strategy - 3:  introduce new node with new version, delete old one, go back online - cloud platform recommend version

## Backup and Restore
* Resource Configs
    ```
    kubectl get all --all-namespaces -o yaml > all-deploy-services.yaml
    ```

* ETCD Cluster
    * Backup
    ```
    etcdctl snapshot save snapshot.db
    etcdctl snapshot status snapshot.db
    ```
    * Restore
    ```
    service kube-apiserver stop
    etcdctl snapshot restore snapshot.db...
    systemctl daemon-reload
    service etcd restart
    service kube-apiserver start
    ```



