# Install Minikube for local Kubernetes Development

## Setting up Virtualization

* Check if **Virtualization** is supported on Windows.
```powershell
systeminfo
```

* Install a Hypervisor
---
## Install kubectl
 **What**: allows you to run commands against Kubernetes clusters

[Guide](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

* Checking kubectl working 
```powershell
kubectl
```
---
## Install minikube 
**What**:  a tool that runs a single-node Kubernetes cluster in a virtual machine on your personal computer

[Install Minikube via direct download](https://github.com/kubernetes/minikube/releases/latest)
```powershell
minikube
minikube start
kubectl version
```
---
## Getting Docker Daemon working inside minikube
```powershell
minikube docker-env


& minikube docker-env | Invoke-Expression (Powershell)
@FOR /f "tokens=*" %i IN ('minikube docker-env') DO @%i (CMD)

docker image ls
```
---
## Connecting minikube via virtual machine's ip address (not accessible via localhost)
```powershell
minikube ip
```
