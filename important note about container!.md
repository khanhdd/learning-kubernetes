# Containers

* Containers are not meant to host an OS.
* Containers are meant to run:
    * Specific task
    * Process (instance of a web server, app server, db server)
    * Some kind of computation or analysis 
* Once the task is complete -> container exit
* Container only lives as long as the process inside it is alive.
