# HELM
* Helm is a tool for managing Kubernetes charts. Charts are packages of pre-configured Kubernetes resources.

# Charts
* Curated applications for Kubernetes

## Install helm
```bash

# binary way
wget https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz
tar -zxvf helm-v2.14.3-linux-amd64.tar.gz
sudo mv inux-amd64/helm /usr/local/bin/helm

# script way
curl -LO https://git.io/get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh

# helm start
helm version
helm init
helm repo update

# helm command
helm ls: list all installation
helm delete --purge <install-name>

# install prometheus operator
helm install --namespace monitoring --name monitoring stable/prometheus-operator
```

## Considering Note
- Avoid helm -> with helm it difficult to keep immutable -> server may become snowflakes server