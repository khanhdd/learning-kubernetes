# Kubectl Mystery :

## Mystery

* **services/kubernetes**: expose services, everytime using kubectl -> post command to rest api end point
* **reclaim policy**: default to delete, means when PersistentVolumeClaim delete -> delete StorageClass as well
* **secrets** in k8s: real case use for hidden sensitive data ouput via terminal (small layer of defence), but different config maps (ref course section26-111)

## Most useful command
```bash
# everything defined in k8s cluster on default namespace
kubectl get all
kubectl get po --show-labels -o wide # get pods wide 
kubectl get ns # get k8s namespaces
kubectl get po -n kube-system # get pods in system ns
kubectl get nodes <node> # describe node

kubectl get <resource> <name> -o yaml: output object <name> to yaml format

kubectl apply -f first-pod.yml: starting first-pod

# describe resource in k8s cluster
kubectl describe pod <name>: inspect <name> pod
kubectl describe svc <name>: inspect service <name>

kubectl exec -it <name> <command>: interactive with pod

kubectl delete po -all: delete all pods

# rolling update with deployment
kubectl rollout status deploy <name>: status of rollout update
kubectl rollout history deploy <name>: history all rollout update
kubectl rollout undo deploy <name> --to-revision=<number>: undo update

kubectl edit -n <namespace> <name>: edit <name> metadata yml

# metrics profilingm with enable of metrics-server or heapster
kubectl top pod
kubectl top node
``` 

```kops
kops edit cluster ${NAME}: edit cluster configuration
kops get ig --name ${NAME}: get list instance group (~ auto scaling group)
kops update cluster ${NAME} --yes: apply update on cluster on the cloud
kops edit ig <ig-name> --name ${NAME}: edit instance group configuration
kops rolling-update cluster --yes: rolling update cluster, kill/start new node
kops validate cluster: validate cluster live or not
kubectl get nodes --show-labels: get nodes
```