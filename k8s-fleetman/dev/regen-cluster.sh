#!/usr/bin/env bash

# set -euxo pipefail
set -e -o pipefail

# Create k8s cluster fleetman.k8s.local
kops create cluster --zones ap-southeast-1a,ap-southeast-1b,ap-southeast-1c ${NAME}

# Replace cluster state in s3 with template state 
kops replace -f cluster.yml --state ${KOPS_STATE_STORE} --name ${NAME} --force

# Create k8s secret 
kops create secret --state ${KOPS_STATE_STORE} --name ${NAME} sshpublickey admin -i ~/.ssh/id_rsa.pub

# Start cluster
kops update cluster --state ${KOPS_STATE_STORE} --name ${NAME} --yes

# Check when cluster is online, starting deploy workloads and service
until kops validate cluster &> /dev/null
do
  echo "Waiting for the cluster online ..."
  sleep 30
done

echo -e "\nCluster is online.\n"

echo -e "\nPrepare to deploy workloads and services on kubernetes cluster ...\n"

# kubectl apply -f ./metadata/.
kubectl apply -f ./metadata/mongo-stack.yml
kubectl apply -f ./metadata/services.yml
kubectl apply -f ./metadata/storage.yml
kubectl apply -f ./metadata/workloads.yml
kubectl apply -f ./metadata/rbac.yml

# helm init
# helm repo update
# ./helm-priviledges.sh

# install prometheus operator via helm charts
# helm install --name monitoring --namespace monitoring stable/prometheus-operator

# kubectl edit -n monitoring service/monitoring-grafana



