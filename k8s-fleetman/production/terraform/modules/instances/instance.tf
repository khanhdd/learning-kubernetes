resource "aws_instance" "instance" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"

  
  subnet_id = "${var.public_subnet}"

  # the security group
  vpc_security_group_ids = ["${var.security_groups}"]

  # the public SSH key
  key_name  = "${aws_key_pair.k8s-key.key_name}"
  user_data = "${var.user_data}"

  iam_instance_profile = "${aws_iam_instance_profile.kops-profile.name}"

  tags {
    Name         = "bootstrap"
    Terraform    = "true"
    Environmnent = "${var.environment}"
  }
}

resource "aws_key_pair" "k8s-key" {
  key_name   = "k8s-${var.ENV}"
  public_key = "${file("${path.root}/${var.path_to_public_key}")}"
}


output "instance_id" {
  value = "${aws_instance.instance.id}"
}

output "public_ip" {
  value = "${aws_instance.instance.public_ip}"
}

output "private_ip" {
  value = "${aws_instance.instance.private_ip}"
}