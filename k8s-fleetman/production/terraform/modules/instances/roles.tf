resource "aws_iam_role" "kops-role" {
    name = "kops-role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "kops-profile" {
    name = "kops-profile"
    role = "${aws_iam_role.kops-role.name}"
}


resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
  role       = "${aws_iam_role.kops-role.name}"
  count      = "${length(var.iam_policy_arn)}"
  policy_arn = "${var.iam_policy_arn[count.index]}"
}
