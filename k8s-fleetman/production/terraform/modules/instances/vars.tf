variable "environment" {}
variable "ami" {}
variable "user_data" {}

variable "path_to_public_key" {
  default = "k8s-dev.pub"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "public_subnet" {
  type = "list"
}

variable "security_groups" {}

variable "iam_policy_arn" {
  description = "IAM Policy to be attached to group"
  type = "list"
  default = ["arn:aws:iam::aws:policy/AmazonEC2FullAccess",
             "arn:aws:iam::aws:policy/AmazonRoute53FullAccess",
             "arn:aws:iam::aws:policy/AmazonS3FullAccess",
             "arn:aws:iam::aws:policy/IAMFullAccess",
             "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
             ]
}


