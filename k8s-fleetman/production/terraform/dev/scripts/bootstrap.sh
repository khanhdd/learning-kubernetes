#!/bin/bash
# sleep until instance is ready
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
  sleep 1
done

apt-get update -y
apt-get upgrade -y

# install docker (optional)
# curl -fsSL https://get.docker.com -o get-docker.sh
# sh get-docker.sh
# Use Docker as a non-root user
# sudo usermod -aG docker ubuntu

# install et remote shell  
apt-get install -y software-properties-common
add-apt-repository ppa:jgmath2000/et -y
apt-get update
apt-get install et -y

