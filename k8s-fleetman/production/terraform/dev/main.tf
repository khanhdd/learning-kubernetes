variable "aws_region" {
  default = "ap-southeast-1"
}

provider "aws" {
  region = "${var.aws_region}"
}

terraform {
  backend "s3" {
    bucket = "terraform-bucket-ddxx"
    key    = "dev/terraform"
    region = "ap-southeast-1"
  }
}

locals {
  azs = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  environment = "dev"
  kops_state_bucket_name = "${local.environment}-kops-state-ddxx" 
  // Needs to be a FQDN
  kubernetes_cluster_name = "fleetman.k8s.local"
  ingress_ips             = ["113.185.41.27/32"]
  vpc_name                = "${local.environment}-vpc"

  tags = {
    environment = "${local.environment}"
    terraform   = true
  }

  ami = "ubuntu"

  amis = {
     amazonlinux2 = "ami-01f7527546b557442"
     ubuntu = "ami-03b6f27628a4569c8"
  }

  bootstrap_user_data = "${file("scripts/bootstrap.sh")}"
}

# module "bootstrap-instance" {
#   source        = "../modules/instances"
#   environment   = "${locals.environment}"
#   ami           = "${lookup(locals.amis, locals.ami)}"
#   user_data     = "${locals.bootstrap_user_data}"
#   public_subnet = "${module.main-vpc.public_subnets}"
#   security_groups = "${aws_security_group.bootstrap_ssh.id}"
# }

data "aws_region" "current" {}



