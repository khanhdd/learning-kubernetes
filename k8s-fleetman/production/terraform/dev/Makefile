.SILENT:
.DEFAULT_GOAL := help

COLOR_RESET = \033[0m
COLOR_COMMAND = \033[36m
COLOR_YELLOW = \033[33m
COLOR_GREEN = \033[32m
COLOR_RED = \033[31m

PROJECT := Deployment k8s-fleetman project with Kops

## 1. Prepare a development environment
plan: prepare

## 2. Provisioning a development environment
apply: install

## 3. Create k8s cluster on development environment 
create: create-cluster

## 4. Uninstall a development environment
destroy: uninstall

prepare:
	terraform get -update=true
	terraform init
	terraform plan -out=out.terraform

install:
	terraform apply "out.terraform"

create-cluster:
	cd ../../kubernetes-cluster/ && \
	./regen-cluster.sh && \
	terraform init && \
	terraform plan -out=out.terraform && \ 
	terraform apply "out.terraform"

uninstall:
	terraform destroy
	cd ../../kubernetes-cluster/
	kops delete cluster --name ${CLUSTER_NAME} --state ${STATE}
	terraform destroy

## 0. Prints help message
help:
	printf "\n${COLOR_YELLOW}${PROJECT}\n------\n${COLOR_RESET}"
	awk '/^[a-zA-Z\-\_0-9\.%]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "${COLOR_COMMAND}$$ make %s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST) | sort
	printf "\n"

## Gen key pair
genkey:
	@read -p "Enter key pair name:" name; \
	ssh-keygen -f $$name
