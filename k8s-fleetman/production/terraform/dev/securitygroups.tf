resource "aws_security_group" "k8s_common_http" {
  name   = "${local.environment}_k8s_common_http"
  vpc_id = "${module.dev_vpc.vpc_id}"
  tags   = "${merge(local.tags)}"

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["${local.ingress_ips}"]
  }

  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["${local.ingress_ips}"]
  }
}
# resource "aws_security_group" "bootstrap_ssh" {
#   vpc_id      = "${module.dev_vpc.vpc_id}"
#   name        = "${local.environment}_bootstrap_ssh"
#   tags   = "${merge(local.tags)}"
  
#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#    ingress {
#     from_port   = 2022
#     to_port     = 2022 
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

# resource "aws_security_group" "rds-launch" {
#   vpc_id      = "${var.VPC_ID}"
#   name        = "rds-launch-${var.ENV}"
#   description = "security group that allows ssh and all egress traffic"

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port       = 3306
#     to_port         = 3306
#     protocol        = "tcp"
#     security_groups = ["${aws_security_group.web-dmz.id}"]
#   }

#   tags {
#     Name         = "rds-launch"
#     Terraform    = "true"
#     Environmnent = "${var.ENV}"
#   }
# }



# output "rds_launch_id" {
#   value = "${aws_security_group.rds-launch.id}"
# }
