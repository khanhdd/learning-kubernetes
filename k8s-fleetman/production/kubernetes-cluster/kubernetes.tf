locals = {
  cluster_name                      = "fleetman.k8s.local"
  master_autoscaling_group_ids      = ["${aws_autoscaling_group.master-ap-southeast-1a-masters-fleetman-k8s-local.id}"]
  master_security_group_ids         = ["${aws_security_group.masters-fleetman-k8s-local.id}"]
  masters_role_arn                  = "${aws_iam_role.masters-fleetman-k8s-local.arn}"
  masters_role_name                 = "${aws_iam_role.masters-fleetman-k8s-local.name}"
  node_autoscaling_group_ids        = ["${aws_autoscaling_group.nodes-fleetman-k8s-local.id}"]
  node_security_group_ids           = ["${aws_security_group.nodes-fleetman-k8s-local.id}"]
  node_subnet_ids                   = ["subnet-09d4c914c7f3e447f", "subnet-0b215829785f4a8fe", "subnet-0fb266b29dcf6204c"]
  nodes_role_arn                    = "${aws_iam_role.nodes-fleetman-k8s-local.arn}"
  nodes_role_name                   = "${aws_iam_role.nodes-fleetman-k8s-local.name}"
  region                            = "ap-southeast-1"
  subnet_ap-southeast-1a_id         = "subnet-09d4c914c7f3e447f"
  subnet_ap-southeast-1b_id         = "subnet-0fb266b29dcf6204c"
  subnet_ap-southeast-1c_id         = "subnet-0b215829785f4a8fe"
  subnet_ids                        = ["subnet-00c6cfc5c922a00b7", "subnet-01e8ab7d237d6534f", "subnet-09d4c914c7f3e447f", "subnet-0ab5d6609c91af5a4", "subnet-0b215829785f4a8fe", "subnet-0fb266b29dcf6204c"]
  subnet_utility-ap-southeast-1a_id = "subnet-0ab5d6609c91af5a4"
  subnet_utility-ap-southeast-1b_id = "subnet-01e8ab7d237d6534f"
  subnet_utility-ap-southeast-1c_id = "subnet-00c6cfc5c922a00b7"
  vpc_id                            = "vpc-0e8d7fd2f787dc27d"
}

output "cluster_name" {
  value = "fleetman.k8s.local"
}

output "master_autoscaling_group_ids" {
  value = ["${aws_autoscaling_group.master-ap-southeast-1a-masters-fleetman-k8s-local.id}"]
}

output "master_security_group_ids" {
  value = ["${aws_security_group.masters-fleetman-k8s-local.id}"]
}

output "masters_role_arn" {
  value = "${aws_iam_role.masters-fleetman-k8s-local.arn}"
}

output "masters_role_name" {
  value = "${aws_iam_role.masters-fleetman-k8s-local.name}"
}

output "node_autoscaling_group_ids" {
  value = ["${aws_autoscaling_group.nodes-fleetman-k8s-local.id}"]
}

output "node_security_group_ids" {
  value = ["${aws_security_group.nodes-fleetman-k8s-local.id}"]
}

output "node_subnet_ids" {
  value = ["subnet-09d4c914c7f3e447f", "subnet-0b215829785f4a8fe", "subnet-0fb266b29dcf6204c"]
}

output "nodes_role_arn" {
  value = "${aws_iam_role.nodes-fleetman-k8s-local.arn}"
}

output "nodes_role_name" {
  value = "${aws_iam_role.nodes-fleetman-k8s-local.name}"
}

output "region" {
  value = "ap-southeast-1"
}

output "subnet_ap-southeast-1a_id" {
  value = "subnet-09d4c914c7f3e447f"
}

output "subnet_ap-southeast-1b_id" {
  value = "subnet-0fb266b29dcf6204c"
}

output "subnet_ap-southeast-1c_id" {
  value = "subnet-0b215829785f4a8fe"
}

output "subnet_ids" {
  value = ["subnet-00c6cfc5c922a00b7", "subnet-01e8ab7d237d6534f", "subnet-09d4c914c7f3e447f", "subnet-0ab5d6609c91af5a4", "subnet-0b215829785f4a8fe", "subnet-0fb266b29dcf6204c"]
}

output "subnet_utility-ap-southeast-1a_id" {
  value = "subnet-0ab5d6609c91af5a4"
}

output "subnet_utility-ap-southeast-1b_id" {
  value = "subnet-01e8ab7d237d6534f"
}

output "subnet_utility-ap-southeast-1c_id" {
  value = "subnet-00c6cfc5c922a00b7"
}

output "vpc_id" {
  value = "vpc-0e8d7fd2f787dc27d"
}

provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_autoscaling_attachment" "master-ap-southeast-1a-masters-fleetman-k8s-local" {
  elb                    = "${aws_elb.api-fleetman-k8s-local.id}"
  autoscaling_group_name = "${aws_autoscaling_group.master-ap-southeast-1a-masters-fleetman-k8s-local.id}"
}

resource "aws_autoscaling_group" "master-ap-southeast-1a-masters-fleetman-k8s-local" {
  name                 = "master-ap-southeast-1a.masters.fleetman.k8s.local"
  launch_configuration = "${aws_launch_configuration.master-ap-southeast-1a-masters-fleetman-k8s-local.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["subnet-09d4c914c7f3e447f"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "fleetman.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-ap-southeast-1a.masters.fleetman.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_autoscaling_group" "nodes-fleetman-k8s-local" {
  name                 = "nodes.fleetman.k8s.local"
  launch_configuration = "${aws_launch_configuration.nodes-fleetman-k8s-local.id}"
  max_size             = 5
  min_size             = 3
  vpc_zone_identifier  = ["subnet-09d4c914c7f3e447f", "subnet-0fb266b29dcf6204c", "subnet-0b215829785f4a8fe"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "fleetman.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "nodes.fleetman.k8s.local"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/node"
    value               = "1"
    propagate_at_launch = true
  }

  metrics_granularity = "1Minute"
  enabled_metrics     = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_ebs_volume" "a-etcd-events-fleetman-k8s-local" {
  availability_zone = "ap-southeast-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "a.etcd-events.fleetman.k8s.local"
    "k8s.io/etcd/events"                       = "a/a"
    "k8s.io/role/master"                       = "1"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_ebs_volume" "a-etcd-main-fleetman-k8s-local" {
  availability_zone = "ap-southeast-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "a.etcd-main.fleetman.k8s.local"
    "k8s.io/etcd/main"                         = "a/a"
    "k8s.io/role/master"                       = "1"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_elb" "api-fleetman-k8s-local" {
  name = "api-fleetman-k8s-local-tkmafs"

  listener = {
    instance_port     = 443
    instance_protocol = "TCP"
    lb_port           = 443
    lb_protocol       = "TCP"
  }

  security_groups = ["${aws_security_group.api-elb-fleetman-k8s-local.id}", "sg-05f1d0764331720ed"]
  subnets         = ["subnet-00c6cfc5c922a00b7", "subnet-01e8ab7d237d6534f", "subnet-0ab5d6609c91af5a4"]

  health_check = {
    target              = "SSL:443"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 10
    timeout             = 5
  }

  idle_timeout = 300

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "api.fleetman.k8s.local"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_iam_instance_profile" "masters-fleetman-k8s-local" {
  name = "masters.fleetman.k8s.local"
  role = "${aws_iam_role.masters-fleetman-k8s-local.name}"
}

resource "aws_iam_instance_profile" "nodes-fleetman-k8s-local" {
  name = "nodes.fleetman.k8s.local"
  role = "${aws_iam_role.nodes-fleetman-k8s-local.name}"
}

resource "aws_iam_role" "masters-fleetman-k8s-local" {
  name               = "masters.fleetman.k8s.local"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_masters.fleetman.k8s.local_policy")}"
}

resource "aws_iam_role" "nodes-fleetman-k8s-local" {
  name               = "nodes.fleetman.k8s.local"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_nodes.fleetman.k8s.local_policy")}"
}

resource "aws_iam_role_policy" "masters-fleetman-k8s-local" {
  name   = "masters.fleetman.k8s.local"
  role   = "${aws_iam_role.masters-fleetman-k8s-local.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_masters.fleetman.k8s.local_policy")}"
}

resource "aws_iam_role_policy" "nodes-fleetman-k8s-local" {
  name   = "nodes.fleetman.k8s.local"
  role   = "${aws_iam_role.nodes-fleetman-k8s-local.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_nodes.fleetman.k8s.local_policy")}"
}

resource "aws_key_pair" "kubernetes-fleetman-k8s-local-200e671406ef7303eaaef78030265172" {
  key_name   = "kubernetes.fleetman.k8s.local-20:0e:67:14:06:ef:73:03:ea:ae:f7:80:30:26:51:72"
  public_key = "${file("${path.module}/data/aws_key_pair_kubernetes.fleetman.k8s.local-200e671406ef7303eaaef78030265172_public_key")}"
}

resource "aws_launch_configuration" "master-ap-southeast-1a-masters-fleetman-k8s-local" {
  name_prefix                 = "master-ap-southeast-1a.masters.fleetman.k8s.local-"
  image_id                    = "ami-0fab8fcc9014a2cbc"
  instance_type               = "c4.large"
  key_name                    = "${aws_key_pair.kubernetes-fleetman-k8s-local-200e671406ef7303eaaef78030265172.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-fleetman-k8s-local.id}"
  security_groups             = ["${aws_security_group.masters-fleetman-k8s-local.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-ap-southeast-1a.masters.fleetman.k8s.local_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_launch_configuration" "nodes-fleetman-k8s-local" {
  name_prefix                 = "nodes.fleetman.k8s.local-"
  image_id                    = "ami-0fab8fcc9014a2cbc"
  instance_type               = "t2.medium"
  key_name                    = "${aws_key_pair.kubernetes-fleetman-k8s-local-200e671406ef7303eaaef78030265172.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes-fleetman-k8s-local.id}"
  security_groups             = ["${aws_security_group.nodes-fleetman-k8s-local.id}"]
  associate_public_ip_address = false
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_nodes.fleetman.k8s.local_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 128
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }

  enable_monitoring = false
}

resource "aws_security_group" "api-elb-fleetman-k8s-local" {
  name        = "api-elb.fleetman.k8s.local"
  vpc_id      = "vpc-0e8d7fd2f787dc27d"
  description = "Security group for api ELB"

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "api-elb.fleetman.k8s.local"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_security_group" "masters-fleetman-k8s-local" {
  name        = "masters.fleetman.k8s.local"
  vpc_id      = "vpc-0e8d7fd2f787dc27d"
  description = "Security group for masters"

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "masters.fleetman.k8s.local"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_security_group" "nodes-fleetman-k8s-local" {
  name        = "nodes.fleetman.k8s.local"
  vpc_id      = "vpc-0e8d7fd2f787dc27d"
  description = "Security group for nodes"

  tags = {
    KubernetesCluster                          = "fleetman.k8s.local"
    Name                                       = "nodes.fleetman.k8s.local"
    "kubernetes.io/cluster/fleetman.k8s.local" = "owned"
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.masters-fleetman-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.masters-fleetman-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "api-elb-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.api-elb-fleetman-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "https-elb-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.api-elb-fleetman-k8s-local.id}"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "master-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.masters-fleetman-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-to-master-tcp-1-2379" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port                = 1
  to_port                  = 2379
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-2382-4000" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port                = 2382
  to_port                  = 4000
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-4003-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port                = 4003
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-udp-1-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-fleetman-k8s-local.id}"
  source_security_group_id = "${aws_security_group.nodes-fleetman-k8s-local.id}"
  from_port                = 1
  to_port                  = 65535
  protocol                 = "udp"
}

terraform = {
  required_version = ">= 0.9.3"
}
